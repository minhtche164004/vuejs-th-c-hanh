import { createStore } from "vuex"
import * as types from "./mutation-types"
import { useToast } from "vue-toastification"
import { format } from "date-fns";
const store = createStore({
    state() {
        return {
            loading: false,
            cart: [],
        }
    },
    getters: {
        getCount(state) {
            return Math.floor(state.count * Math.random())
        },
        getCartLength(state) {
            return state.cart.length
        }
    },
    mutations: {
        increment(state, payload) {
            if (state.cart.find(item => item.id === payload.id)) {
                state.cart.map(item => {
                    item.quantity++
                    item.time = format(new Date(), "HH:mm:ss dd/MM/yyyy")
                    return item
                })
            } else {
                state.cart.push({ ...payload, time: format(new Date(), "HH:mm:ss dd/MM/yyyy"), quantity: 1 })
            }
            console.log(state.cart)
        },
        setLoading(state, isLoading) {
            state.loading = isLoading;
            console.log(isLoading)
        },
        [types.ADD_TO_CART](state, amount) {
            state.count += amount
        },
        getCart(state, payload) {
            console.log(payload)
            state.cart = payload
        },
        delete(state, payload) {
            state.cart = state.cart.filter(item => item.id !== payload)
            console.log(state.cart)
        }
    },
    actions: {
        increment({ commit }, payload) {
            // Trước khi bắt đầu tăng giá trị, đặt trạng thái loading là true
            commit('setLoading', true);

            setTimeout(() => {
                commit('increment', payload);

                // Sau khi commit hoàn thành, đặt trạng thái loading là false
                commit('setLoading', false);
                useToast().success('Thêm vào giỏ hàng thành công!', { timeout: 2000 })
            }, 2000);
        },
        delete({ commit }, payload) {
            commit('delete', payload)
        }
    }
})
export default store